<?php
global $base_url;
$hg_uri = variable_get('d4os_default_hg_uri', $base_url);
$hg_port = variable_get('d4os_default_hg_port', '8002');
?>
<html>
  <head>
    <meta charset="utf-8" />
    <title>Destination guide</title>
    <style>
      body {
        background-color: #000;
        font-family: arial;
        color: #FFF;
      }
      a {
        color: #FFF;
        text-decoration: none;
      }
      ul {
        list-style-type: none;
        text-align: center;
      }
      li.title {
        font-size: 12px;
      }
      li.tp a,
      li.hgtp a {
        border: 1px solid grey;
        border-radius: 16px;
        padding: 2px;
        display: block;
        background-color: #333;
        margin: 4px;
        font-size: 8px;
      }
      li.tp a:hover,
      li.hgtp a:hover {
        background-color: #777;
      }
    </style>
  </head>
  <body>
    <table>
      <tr>
        <?php foreach ($variables['regions'] as $region): ?>
        <td nowrap>
          <ul>
            <li class="image"><img title="<?php print $region->values['description']; ?>" src="<?php print d4os_get_texture($region->values['snapshot_uuid'], 'png', 100) ?>" width="100px" height="100px"/></li>
            <li class="title"><a href="#" title="<?php print $region->values['description']; ?>"><?php print $region->values['title']; ?></a></li>
            <li class="tp"><a href="secondlife:///app/teleport/<?php print urlencode($region->values['title']); ?>/<?php print d4os_convert_vector_to_destination($region->values['landing_point']); ?>" title="Teleport">TP</a></li>
            <li class="hgtp"><a href="secondlife:///app/teleport/<?php print $hg_uri; ?>:<?php print $hg_port; ?>:<?php print urlencode($region->values['title']); ?>" title="Hypergrid teleport">HGTP</a></li>
          </ul>
        </td>
        <?php endforeach; ?>
      </tr>
    </table>
</body>
</html>
